#!/bin/sh -e

VERSION=$2
TAR=../jcaptcha_$VERSION.orig.tar.gz
DIR=jcaptcha-$VERSION
TAG=$(echo "V$VERSION" | sed -re's/~(alpha|beta)/_\1_/' | sed -re's/\./_/')

svn export https://jcaptcha.svn.sourceforge.net/svnroot/jcaptcha/tags/${TAG}/ $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' \
     --exclude 'src/doc/captcha_crypt.pdf' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
